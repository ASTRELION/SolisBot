﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;

namespace SolisBot
{
    [Remarks("1")]
    public class General : ModuleBase
    {
        private IServiceProvider map;

        public General(IServiceProvider map)
        {
            this.map = map;
        }
        
        // >emoji
        [Group("Emoji")]
        [Alias("e")]
        public class Emoji : ModuleBase
        {
            // >emoji lenny
            [Command("lenny")]
            [Summary("Displays ( ͡° ͜ʖ ͡°)")]
            [Alias("len")]
            public async Task Lenny()
            {
                await Context.Message.ModifyAsync(x => x.Content = "( ͡° ͜ʖ ͡°)");
            }

            // >emoji tableflip 
            [Command("tableflip")]
            [Summary("Displays (╯°□°）╯︵ ┻━┻")]
            [Alias("tf")]
            public async Task TableFlip()
            {
                await Context.Message.ModifyAsync(x => x.Content = "(╯°□°）╯︵ ┻━┻");
            }

            // >emoji tablereset 
            [Command("tablereset")]
            [Summary("Displays ┬─┬﻿ ノ( ゜-゜ノ)")]
            [Alias("tr")]
            public async Task TableReset()
            {
                await Context.Message.ModifyAsync(x => x.Content = "┬─┬﻿ ノ( ゜-゜ノ)");
            }

            // >emoji idc 
            [Command("dontcare")]
            [Summary(@"Displays ¯\_(ツ)_/¯")]
            [Alias("idc")]
            public async Task IDC()
            {
                await Context.Message.ModifyAsync(x => x.Content = @"¯\_(ツ)_/¯");
            }

            // >emoji seal 
            [Command("seal")]
            [Summary("Displays ᶘ ᵒᴥᵒᶅ")]
            public async Task Seal()
            {
                await Context.Message.ModifyAsync(x => x.Content = "ᶘ ᵒᴥᵒᶅ");
            }
        }

        // >text
        [Group("Text")]
        [Alias("t")]
        public class Text : ModuleBase
        {
            [Command("style")]
            [Summary("Styles text")]
            [Alias("s")]
            public async Task Style(
                string style,
                [Remainder] string text)
            {
                style = style.ToLower();
                if (style.Contains("i")) text = $"*{text}*";
                if (style.Contains("b")) text = $"**{text}**";
                if (style.Contains("u")) text = $"__{text}__";
                if (style.Contains("s")) text = $"~~{text}~~";

                await Context.Message.ModifyAsync(x => x.Content = text);
            }

            [Command("codeblock")]
            [Summary("Styles text in a code block")]
            [Alias("cb")]
            public async Task CodeBlock(
                [Remainder] string text)
            {
                await Context.Message.ModifyAsync(x => x.Content = $"```{text}```");
            }

            // >text altcaps
            [Command("altcaps")]
            [Summary("Alternates the capitalization of a message")]
            [Alias("altc", "altcap")]
            public async Task AltCaps(
                [Remainder] string msg)
            {
                string newMsg = "";

                for (int i = 0; i < msg.Length; i++)
                {
                    newMsg += i % 2 == 0 ? msg[i].ToString().ToLower() : msg[i].ToString().ToUpper();
                }

                await Context.Message.ModifyAsync(x => x.Content = newMsg);
            }

            // >text addspace
            [Command("addspace")]
            [Summary("Adds spaces between every letter of a message")]
            [Alias("adds")]
            public async Task AddSpace(
                [Remainder] string msg)
            {
                string newMsg = "";

                foreach (char c in msg)
                {
                    newMsg += c + " ";
                }

                await Context.Message.ModifyAsync(x => x.Content = newMsg);
            }

            // >text emojireplace
            [Command("emojireplace")]
            [Summary("Replaces every letter with an emoji")]
            [Alias("emojir")]
            public async Task EmojiReplace(
                [Remainder] string msg)
            {
                string newMsg = "";

                foreach (char c in msg)
                {
                    if ("abcdefghijklmnopqrstuvwxyz".Contains(c.ToString().ToLower()))
                        newMsg += $":regional_indicator_{c.ToString().ToLower()}:";
                    else
                        newMsg += c;
                }

                await Context.Message.ModifyAsync(x => x.Content = newMsg);
            }
        }

        [Command("game")]
        [Summary("Set your game as given text")]
        public async Task Game([Remainder] string text)
        {
            DiscordSocketClient client = map.GetService<DiscordSocketClient>();
            await client.SetGameAsync(text);
        }

        [Command("lookup")]
        [Summary("Lookup a Discord ID")]
        public async Task Lookup(ulong id)
        {
            DiscordSocketClient client = map.GetService<DiscordSocketClient>();
            
            EmbedBuilder embed = new EmbedBuilder()
            {
                Title = "ID " + id.ToString(),
                Color = SolisUtil.offWhite
            };
            EmbedFieldBuilder field1 = new EmbedFieldBuilder();
            EmbedFieldBuilder field2 = new EmbedFieldBuilder();
            
            if (client.GetGuild(id) != null)
            {
                field1 = new EmbedFieldBuilder()
                {
                    Name = "Type",
                    Value = "Guild",
                    IsInline = true
                };

                field2 = new EmbedFieldBuilder()
                {
                    Name = "Name",
                    Value = client.GetGuild(id).Name,
                    IsInline = true
                };
            }  
            else if (client.GetChannel(id) != null)
            {
                field1 = new EmbedFieldBuilder()
                {
                    Name = "Type",
                    Value = "Channel",
                    IsInline = true
                };

                field2 = new EmbedFieldBuilder()
                {
                    Name = "Name",
                    Value = client.GetChannel(id),
                    IsInline = true
                };
            }   
            else if (client.GetUser(id) != null)
            {
                field1 = new EmbedFieldBuilder()
                {
                    Name = "Type",
                    Value = "User",
                    IsInline = true
                };

                field2 = new EmbedFieldBuilder()
                {
                    Name = "User Name",
                    Value = client.GetUser(id).Username + "#" + client.GetUser(id).DiscriminatorValue,
                    IsInline = true
                };
            }
            else
            {
                SolisUtil.CommandFailed(Context.Message, "No such Discord ID.");
            }

            embed.AddField(field1);
            embed.AddField(field2);
            await Context.Message.ModifyAsync(x => x.Content = "\u200B");
            await Context.Message.ModifyAsync(x => x.Embed = embed.Build());
        }

        [Command("invite")]
        [Summary("Send invite to server")]
        public async Task Invite(
            [Remainder, Summary("Name of server, or none for this server")] string serverName = "")
        {
            await Context.Message.DeleteAsync();
            serverName = serverName == "" ? Context.Guild.Name : serverName;

            DiscordSocketClient client = map.GetService<DiscordSocketClient>();
            SocketGuild guild = client.Guilds.Where(x => x.Name.ToLower().Equals(serverName.ToLower())).FirstOrDefault();
            
            if (guild.GetUser(Context.User.Id).GuildPermissions.Administrator)
            {
                var infiniteInvites = guild.GetInvitesAsync().Result.Where(x => x.IsTemporary == false);
                var userInvites = infiniteInvites.Where(x => x.Inviter.Id == Context.User.Id).FirstOrDefault();

                await ReplyAsync(userInvites == null ? infiniteInvites.FirstOrDefault().ToString() : userInvites.ToString());
            }
            else
            {
                SolisUtil.CommandFailed(Context.Message, "You do not have permission to get an invite to the specified server.");
            }
        }

        [Command("stats")]
        [Summary("Get stats about the bot")]
        public async Task Stats()
        {
            await Context.Message.DeleteAsync();
            DiscordSocketClient client = map.GetService<DiscordSocketClient>();
            TempData data = map.GetService<TempData>();

            EmbedBuilder embed = new EmbedBuilder()
            {
                Author = new EmbedAuthorBuilder()
                {
                    Name = "Stats",
                    IconUrl = Context.User.GetAvatarUrl()
                },
                Color = SolisUtil.offWhite
            };
            
            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":outbox_tray: Messages Sent",
                Value = data.messagesSent,
                IsInline = true
            });

            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":inbox_tray: Messages Received",
                Value = data.messagesReceved,
                IsInline = true
            });

            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":keyboard: Commands Issued",
                Value = data.commandsIssued,
                IsInline = true
            });

            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":exclamation: Mentions",
                Value = data.mentions,
                IsInline = true
            });

            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":crossed_swords: Servers",
                Value = client.Guilds.Count.ToString(),
                IsInline = true
            });

            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":video_game: Game",
                Value = (Context.User.Game == null || Context.User.Game.ToString() == "") ? "None" : Context.User.Game.ToString(),
                IsInline = true
            });

            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":arrow_up: Bot Uptime",
                Value = (DateTime.Now - data.startTime).ToString("hh':'mm':'ss"),
                IsInline = true
            });

            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":birthday: User Creation",
                Value = Context.User.CreatedAt.LocalDateTime.ToString(),
                IsInline = true
            });

            TimeSpan age = DateTime.Now - Context.User.CreatedAt;
            embed.AddField(new EmbedFieldBuilder()
            {
                Name = ":clock3: User Age",
                Value = $"{age.Days} days ({Math.Round(age.Days / 365.0, 2)} years)",
                IsInline = true
            });

            await ReplyAsync("", embed: embed);
        }

        [Command("server")]
        [Summary("Get info about the current server")]
        public async Task Server(string serverName = "")
        {
            DiscordSocketClient client = map.GetService<DiscordSocketClient>();
            IGuild guild = client.Guilds.Where(x => x.Name.ToLower().Equals(serverName.ToLower())).FirstOrDefault();
            await Server(guild.Id);
        }

        [Command("server")]
        [Summary("Get info about the current server")]
        public async Task Server(
            [Summary("ID of server")] ulong id = 0)
        {
            await Context.Message.DeleteAsync();

            if (Context.Guild != null)
            {
                DiscordSocketClient client = map.GetService<DiscordSocketClient>();
                IGuild guild = client.GetGuild(id) == null ? Context.Guild : client.GetGuild(id);
                IGuildUser owner = await guild.GetOwnerAsync();
                
                EmbedBuilder embed = new EmbedBuilder()
                {
                    Author = new EmbedAuthorBuilder()
                    {
                        Name = guild.Name,
                        IconUrl = guild.IconUrl
                    },
                    ThumbnailUrl = guild.IconUrl,
                    Color = SolisUtil.offWhite
                };

                EmbedFieldBuilder ownerName = new EmbedFieldBuilder()
                {
                    Name = ":busts_in_silhouette: Owner",
                    Value = $"{owner.Username}#{owner.Discriminator}",
                    IsInline = false
                };

                EmbedFieldBuilder online = new EmbedFieldBuilder()
                {
                    Name = ":small_blue_diamond: Online Users",
                    Value = (await guild.GetUsersAsync()).Where(x => x.Status != UserStatus.Offline).Count(),
                    IsInline = true
                };

                EmbedFieldBuilder totalUsers = new EmbedFieldBuilder()
                {
                    Name = ":small_orange_diamond: Total Users",
                    Value = (await guild.GetUsersAsync()).Count,
                    IsInline = true
                };

                EmbedFieldBuilder textChannels = new EmbedFieldBuilder()
                {
                    Name = ":abcd: Text Channels",
                    Value = (await guild.GetTextChannelsAsync()).Count,
                    IsInline = true
                };

                EmbedFieldBuilder voiceChannels = new EmbedFieldBuilder()
                {
                    Name = ":mega: Voice Channels",
                    Value = (await guild.GetVoiceChannelsAsync()).Count,
                    IsInline = true
                };

                EmbedFieldBuilder region = new EmbedFieldBuilder()
                {
                    Name = ":mountain: Region",
                    Value = guild.VoiceRegionId,
                    IsInline = true
                };

                EmbedFieldBuilder verification = new EmbedFieldBuilder()
                {
                    Name = ":lock: Verification Level",
                    Value = guild.VerificationLevel.ToString(),
                    IsInline = true
                };

                EmbedFieldBuilder creationDate = new EmbedFieldBuilder()
                {
                    Name = ":birthday: Creation Date",
                    Value = guild.CreatedAt.ToLocalTime().Date.ToLongDateString(),
                    IsInline = true
                };

                EmbedFieldBuilder age = new EmbedFieldBuilder()
                {
                    Name = ":clock3: Age",
                    Value = (DateTime.Now - guild.CreatedAt).Days + " days",
                    IsInline = true
                };

                embed.AddField(ownerName);
                embed.AddField(online);
                embed.AddField(totalUsers);
                embed.AddField(textChannels);
                embed.AddField(voiceChannels);
                embed.AddField(region);
                embed.AddField(verification);
                embed.AddField(creationDate);
                embed.AddField(age);

                await ReplyAsync("", embed: embed);
            }
            else
            {
                SolisUtil.CommandFailed(Context.Message, "This command cannot be used when not in a server.");
            }
        }

        [Command("info")]
        [Summary("Get info about a user")]
        [Alias("i")]
        public async Task Info(
            [Summary("User to get information about")] IUser user = null)
        {
            await Context.Message.DeleteAsync();

            user = user == null ? Context.User : user;

            EmbedBuilder embed = new EmbedBuilder()
            {
                ThumbnailUrl = user.GetAvatarUrl(),
                Author = new EmbedAuthorBuilder()
                {
                    Name = $"{user.Username}#{user.Discriminator}",
                    IconUrl = user.GetAvatarUrl()
                },
                Color = SolisUtil.offWhite
            };

            EmbedFieldBuilder status = new EmbedFieldBuilder()
            {
                Name = ":vertical_traffic_light: Status",
                Value = user.Status,
                IsInline = true
            };

            EmbedFieldBuilder creationDate = new EmbedFieldBuilder()
            {
                Name = ":birthday: User Creation",
                Value = user.CreatedAt.ToLocalTime().Date.ToLongDateString(),
                IsInline = true
            };

            EmbedFieldBuilder game = new EmbedFieldBuilder()
            {
                Name = ":video_game: Game",
                Value = user.Game == null || user.Game.ToString() == "" ? "None" : user.Game.ToString(),
                IsInline = true
            };

            embed.AddField(status);
            embed.AddField(game);

            if (Context.Guild != null)
            {
                embed.Author = new EmbedAuthorBuilder()
                {
                    Name = $"{user.Username}#{user.Discriminator}",
                    IconUrl = Context.Guild.IconUrl
                };

                EmbedFieldBuilder nickname = new EmbedFieldBuilder()
                {
                    Name = ":busts_in_silhouette: Nickname",
                    Value = ((IGuildUser)user).Nickname,
                    IsInline = true
                };

                EmbedFieldBuilder channel = new EmbedFieldBuilder()
                {
                    Name = ":mega: Channel",
                    Value = ((IGuildUser)user).VoiceChannel == null ? "None" : ((IGuildUser)user).VoiceChannel.Name,
                    IsInline = true
                };

                EmbedFieldBuilder joinDate = new EmbedFieldBuilder()
                {
                    Name = ":calendar_spiral: Server Join",
                    Value = ((IGuildUser)user).JoinedAt.Value.ToLocalTime().Date.ToLongDateString(),
                    IsInline = true
                };

                embed.AddField(nickname);
                embed.AddField(channel);
                embed.AddField(joinDate);
            }

            embed.AddField(creationDate);

            await ReplyAsync("", embed: embed);
        }

        // >help
        [Command("help")]
        [Summary("List all commands you have access to")]
        [Alias("h", "?")]
        public async Task Help()
        {
            List<ModuleInfo> moduleList = map.GetService<List<ModuleInfo>>();

            var message = await ReplyAsync(Context.User.Mention, embed: SolisUtil.GetHelpPage(1, moduleList, Context as CommandContext));

            for (int i = 0; i < moduleList.Count; i++)
            {
                await message.AddReactionAsync(new Discord.Emoji((i + 1) + "\u20e3"));
            }
        }

        // >help
        [Command("help")]
        [Summary("Get help on a specific command")]
        [Alias("h", "?")]
        public async Task Help(
            [Summary("Command to get help with"), Remainder] string command)
        {
            CommandService commands = map.GetService<CommandService>();

            try
            {
                var cmds = commands.Commands.Where(x => x.Name.ToUpper().Equals(command.ToUpper())).ToList();

                foreach (var cmd in cmds)
                {
                    EmbedBuilder e = new EmbedBuilder()
                    {
                        Title = $"**!{cmd.Name} Help**",
                        Description = $"{cmd.Summary}",
                        Color = SolisUtil.offWhite
                    };

                    EmbedFieldBuilder syntax = new EmbedFieldBuilder()
                    {
                        Name = "Command Syntax",
                        Value = $"{SolisUtil.GetCommandSyntax(cmd)}\t\t\u200B",
                        IsInline = true
                    };

                    EmbedFieldBuilder alias = new EmbedFieldBuilder()
                    {
                        Name = "Command Alias(es)",
                        Value = "\u200B",
                        IsInline = true
                    };

                    for (int i = 0; i < cmd.Aliases.Count; i++)
                    {
                        alias.Value += $"!{cmd.Aliases[i]}" + (i == cmd.Aliases.Count - 1 ? "" : ", ");
                    }

                    e.AddField(syntax);
                    e.AddField(alias);

                    if (cmd.Parameters.Count > 0)
                    {
                        EmbedFieldBuilder param = new EmbedFieldBuilder()
                        {
                            Name = "\u200B",
                            Value = "**Command Parameter(s)**",
                            IsInline = false
                        };

                        e.AddField(param);

                        foreach (var p in cmd.Parameters)
                        {
                            EmbedFieldBuilder par = new EmbedFieldBuilder()
                            {
                                Name = p.Name,
                                Value = $"{p.Summary}" +
                                        $"\nRequired? {!p.IsOptional}",
                                IsInline = true
                            };

                            e.AddField(par);
                        }
                    }

                    await ReplyAsync("", embed: e);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        // >ping
        [Command("ping")]
        [Summary("Ping the bot")]
        public async Task Ping()
        {
            
            DiscordSocketClient client = map.GetService<DiscordSocketClient>();
            var m = await ReplyAsync($"Pong! {client.Latency}ms");
        }
    }
}