﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace SolisBot
{
    public class TempData
    {
        public int messagesSent { get; set; } = 0;
        public int messagesReceved { get; set; } = 0;
        public int commandsIssued { get; set; } = 0;
        public int mentions { get; set; } = 0;

        public DateTime startTime { get; set; } = DateTime.Now;
    }
}
