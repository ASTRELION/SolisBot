﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Net;
using Discord.Commands;
using Discord.WebSocket;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace SolisBot
{
    public class Program
    {
        private DiscordSocketClient client;
        private CommandService commands;
        private IServiceProvider service;
        private List<ModuleInfo> moduleList = new List<ModuleInfo>();
        private TempData data = new TempData();

        static void Main(string[] args) =>
            new Program().Start().GetAwaiter().GetResult();

        public async Task Start()
        {
            // Initialize client
            client = new DiscordSocketClient();
            // Events
            client.Log += Log;

            client.MessageReceived += MessageReceivedEvent;

            client.Ready += ReadyEvent;

            commands = new CommandService();
            await InstallCommands();

            // Dependency injection
            service = ConfigureServices();

            // Connect to discord
            await client.LoginAsync(TokenType.User, "");
            await client.StartAsync();


            await Task.Delay(-1); // Infinite delay
        }

        public async Task InstallCommands()
        {
            client.MessageReceived += HandleCommand;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        public async Task HandleCommand(SocketMessage msg)
        {
            int argPos = 0;
            var message = msg as SocketUserMessage;

            if (message == null ||
                message.Author.Id != client.CurrentUser.Id ||
                !message.HasCharPrefix('>', ref argPos)) return;

            var context = new CommandContext(client, message);
            var result = await commands.ExecuteAsync(context, argPos, service);

            if (!result.IsSuccess)
            {
                Console.WriteLine(message + " : (" + result.Error + ") " + result.ErrorReason);
                await message.DeleteAsync();
            }
        }

        private IServiceProvider ConfigureServices()
        {
            foreach (var module in commands.Modules)
            {
                if (module.Parent == null) // Is own page
                {
                    moduleList.Add(module);
                }
            }

            moduleList = moduleList.OrderBy(x => int.Parse(x.Remarks)).ToList();

            return new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(commands)
                .AddSingleton(moduleList)
                .AddSingleton(data)
                .BuildServiceProvider();
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        private async Task ReadyEvent()
        {
            IApplication bot = await client.GetApplicationInfoAsync();
            await client.CurrentUser.ModifyAsync(x =>
            {
                x.Username = bot.Name;
            });

            await client.SetGameAsync($"testing");
        }

        private async Task MessageReceivedEvent(SocketMessage msg)
        {
            if (msg.Author.Id == client.CurrentUser.Id)
            {
                data.messagesSent++;

                if (msg.Content.StartsWith(">"))
                {
                    data.commandsIssued++;
                }
            }
            else
            {
                data.messagesReceved++;
            }

            if (msg.MentionedUsers.Any(x => x.Id == client.CurrentUser.Id))
            {
                data.mentions++;
            }
        }
    }
}
