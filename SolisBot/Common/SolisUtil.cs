﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace SolisBot
{
    public class SolisUtil
    {
        public static Color lightBlue = new Color(0, 200, 255);
        public static Color offWhite = new Color(210, 210, 210);
        public static Color red = new Color(255, 0, 0);
        public static Color yellow = new Color(255, 255, 0);
        public static Color green = new Color(0, 255, 0);
        public static Color black = new Color(1, 1, 1);

        public static void CommandFailed(IUserMessage msg, string failureMsg = "")
        {
            string message = "";

            if(failureMsg == null || failureMsg == "")
            {
                message = $":x: command '{msg.Content.Substring(msg.Content.IndexOf('>') + 1)}' failed";
            }
            else
            {
                message = $":x: {failureMsg}";
            }

            msg.Channel.SendMessageAsync("", embed: (new EmbedBuilder()
            {
                Title = message,
                Color = red
            }).Build());
        }

        public static void CommandSuccess(IUserMessage msg, string successMsg = "")
        {
            string message = "";

            if (successMsg == null || successMsg == "")
            {
                message = $":white_check_mark: command '{msg.Content.Substring(msg.Content.IndexOf('>') + 1)}' succeeded";
            }
            else
            {
                message = $":white_check_mark: {successMsg}";
            }

            msg.Channel.SendMessageAsync("", embed: (new EmbedBuilder()
            {
                Title = message,
                Color = green
            }).Build());
        }

        public static Embed GetHelpPage(int pageNo, List<ModuleInfo> moduleList, CommandContext context)
        {
            ModuleInfo page;

            if (pageNo >= 1 && pageNo <= moduleList.Count)
            {
                page = moduleList[pageNo - 1];
                List<CommandInfo> parentsCommands = page.Commands.OrderBy(x => x.Aliases[0]).Where(x => x.CheckPreconditionsAsync(context).Result.IsSuccess).ToList();
                List<ModuleInfo> subModules = page.Submodules.OrderBy(x => x.Name).ToList();

                EmbedBuilder embedPage = new EmbedBuilder()
                {
                    Title = $"{page.Name} Help",
                    Description = "\u200B",
                    Footer = new EmbedFooterBuilder()
                    {
                        Text = $"!help | Page {pageNo} / {moduleList.Count}"
                    },
                    Color = offWhite
                };

                foreach (CommandInfo com in parentsCommands)
                {
                    embedPage.Description += $"**{GetCommandSyntax(com)}**";

                    if (com.Summary.Length > 0)
                    {
                        embedPage.Description += $": {com.Summary} ";
                    }

                    embedPage.Description += "\n";
                }

                foreach (ModuleInfo subMod in subModules)
                {
                    EmbedFieldBuilder subField = new EmbedFieldBuilder()
                    {
                        Name = subMod.Name,
                        Value = "\u200B"
                    };

                    BuildField(subMod);

                    void BuildField(ModuleInfo m)
                    {
                        foreach (CommandInfo subCom in m.Commands.Where(x => x.CheckPreconditionsAsync(context).Result.IsSuccess))
                        {
                            subField.Value += $"**{GetCommandSyntax(subCom)}**";

                            if (subCom.Summary.Length > 0)
                            {
                                subField.Value += $": {subCom.Summary} ";
                            }

                            subField.Value += "\n";
                        }

                        foreach (ModuleInfo subSub in m.Submodules)
                        {
                            BuildField(subSub);
                        }
                    }

                    if (!subField.Value.Equals("\u200B"))
                        embedPage.AddField(subField);
                }

                return embedPage.Build();
            }

            return null;
        }

        public static string GetCommandSyntax(CommandInfo command)
        {
            string commandSyntax = $"!{command.Aliases[0]} ";

            if (command.Parameters.Count > 0)
            {
                foreach (ParameterInfo param in command.Parameters)
                {
                    if (param.IsOptional)
                    {
                        commandSyntax += $"[{param.Name}] ";
                    }
                    else
                    {
                        commandSyntax += $"<{param.Name}> ";
                    }
                }
            }

            return commandSyntax;
        }
    }
}
